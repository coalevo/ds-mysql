/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.mysql.impl;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import net.coalevo.datasource.model.DataSourceProvider;
import net.coalevo.datasource.mysql.service.MySQLDataSourceProviderConfiguration;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Implements a {@link DataSourceProvider} for a MySQL
 * Database.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MySQLDataSourceProvider
    implements DataSourceProvider, ConfigurationUpdateHandler {

  private DataSource m_DataSource;
  private Marker m_LogMarker = MarkerFactory.getMarker(getClass().getName());

  public String getIdentifier() {
    return "mysql";
  }//getIdentifier

  public DataSource getDataSource() {
    return m_DataSource;
  }//getDataSource

  public void activate() {
    ConfigurationMediator cm = Activator.getServices().getConfigurationMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();
    m_DataSource = new MysqlDataSource();
    configure(mtd);
  }//activate

  public void deactivate() {
    m_DataSource = null;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    configure(mtd);
  }//update

  private void configure(MetaTypeDictionary mtd) {
    String user = "coalevo";
    String pass = "coalevo07";
    String url = "jdbc:mysql://localhost:3306/coalevo&characterEncoding=UTF-8&connectionCollation=utf8_bin";
    try {
      user = mtd.getString(MySQLDataSourceProviderConfiguration.CONFIG_USERNAME_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "MySQLDataSourceProvider.configexception",
              "attribute",
              MySQLDataSourceProviderConfiguration.CONFIG_USERNAME_KEY),
          ex
      );
    }
    try {
      pass = mtd.getString(MySQLDataSourceProviderConfiguration.CONFIG_PASSWORD_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "MySQLDataSourceProvider.configexception",
              "attribute",
              MySQLDataSourceProviderConfiguration.CONFIG_PASSWORD_KEY),
          ex
      );
    }
    try {
      url = mtd.getString(MySQLDataSourceProviderConfiguration.CONFIG_URL_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get(
              "MySQLDataSourceProvider.configexception",
              "attribute",
              MySQLDataSourceProviderConfiguration.CONFIG_URL_KEY),
          ex
      );
    }
    synchronized (m_DataSource) {
      MysqlDataSource mds = (MysqlDataSource) m_DataSource;
      mds.setUser(user);
      mds.setPassword(pass);
      mds.setURL(url);
      mds.setConnectionCollation("utf8_bin");      
    }
    checkConnection();
  }//configure

  private void checkConnection() {
    try {
      Connection c = m_DataSource.getConnection();
      c.close();
    } catch (SQLException ex) {
      Activator.log().error(m_LogMarker, "checkConnection()", ex);
    }
  }//checkConnection

}//class MySQLDataSourceProvider
