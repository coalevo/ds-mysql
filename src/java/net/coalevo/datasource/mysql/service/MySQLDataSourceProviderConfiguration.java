/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.mysql.service;

/**
 * Defines a tag interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.foundation.util.BundleConfiguration
 */
public interface MySQLDataSourceProviderConfiguration {

  /**
   * Defines the key for the username configuration property.
   */
  public static final String CONFIG_USERNAME_KEY = "username";

  /**
   * Defines the key for the password configuration property.
   */
  public static final String CONFIG_PASSWORD_KEY = "password";

  /**
   * Defines the key for the url configuration property.
   */
  public static final String CONFIG_URL_KEY = "url";

}//interface MySQLDataSourceProviderConfiguration
